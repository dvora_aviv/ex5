<?php

use yii\db\Migration;

class m160510_084848_init_students_table extends Migration
{
    public function up()
    {
		$this->createTable(
			'students',
			[
			'id'=>'pk',
			'name'=>'string',
			],
			'ENGINE=InnoDB'
		);

    }

    public function down()
    {
		$this->dropTable('students');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
